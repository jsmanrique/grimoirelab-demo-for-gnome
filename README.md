# GrimoireLab Demo for GNOME

This repository hosts [GrimoireLab](https://github.com/chaoss/grimoirelab )
configuration files and explanation of how to set up GrimoireLab to analyze 
GNOME Foundation projects

# Requirements

You can run this in your laptop, but I would recommend to run it on a dedicated
server you own, or in one provided by any cloud provider.

I'll explain later how to install it using 
[GrimoireLab Infra Provision](https://gitlab.com/jsmanrique/grimoirelab-infra-provision)
project. For it, you need:
* [Terraform](https://learn.hashicorp.com/terraform/getting-started/install.html).
You need to [download the appropiate package](https://www.terraform.io/downloads.html),
and unzip the package. Terraform runs as a single binary named `terraform`.
* [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html).
I am using Ubuntu, so I have installed it from Ubuntu repositories:
```
sudo apt install ansible
```

Additionally, you need an account in [GNOME's GitLab](https://gitlab.gnome.org/) 
and [an access token](https://gitlab.gnome.org/profile/personal_access_tokens).
The one I am using is set of `api, read_user, read_repository, read_registry`.

Modify [setup.cfg](setup.cfg) file to include such token instead of the 
`<YOUR_GITLAB_GNOME_API_HERE>` placeholder.

# Get started

## Running it by your self in your own server or machine

Check [GrimoireLab Getting Started section](https://github.com/chaoss/grimoirelab#getting-started),
and instead of using the default settings provided there, use the
[projects.json](projects.json) and [setup.cfg](setup.cfg) files provided in
this repository.

## Using GrimoireLab Infra Provision

1. Clone or fork and clone [GrimoireLab Infra Provision](https://gitlab.com/jsmanrique/grimoirelab-infra-provision)
project

```
git clone https://gitlab.com/jsmanrique/grimoirelab-infra-provision
```

2. Set up your environment for your cloud provider (you should end having a machine
ready in a `<PUBLIC_IP>`):
 * [AWS](https://gitlab.com/jsmanrique/grimoirelab-infra-provision/blob/master/aws/README.md)
 * [Digital Ocean](https://gitlab.com/jsmanrique/grimoirelab-infra-provision/blob/master/do/README.md)
 * [GCP](https://gitlab.com/jsmanrique/grimoirelab-infra-provision/blob/master/gcp/README.md)

3. Clone or fork and clone this repository in your computer. Check the path to it.
Let's say it is `<PATH>/grimoirelab-demo-for-gnome`.

4. Go to `ansible` folder in `grimoirelab-infra-provision` and run:
```
ansible-playbook -i <PUBLIC_IP>, deploy.yml -e src_settings_path=<PATH>/grimoirelab-demo-for-gnome
```

This should produce an output like this (where instead of `167.172.129.114` you'll
see your cloud machine's `<PUBLIC_IP>`):
```
PLAY [all] **********************************************************************************************************************************************************************

TASK [Gathering Facts] **********************************************************************************************************************************************************
ok: [167.172.129.114]

TASK [Set sysctl] ***************************************************************************************************************************************************************
 [WARNING]: Consider using 'become', 'become_method', and 'become_user' rather than running sudo

changed: [167.172.129.114]

TASK [Clone GrimoireLab repo] ***************************************************************************************************************************************************
changed: [167.172.129.114]

TASK [Set down running GrimoireLab containers] **********************************************************************************************************************************
changed: [167.172.129.114]

TASK [Set down GrimoireLab containers in GCP Containers Optimized instance] *****************************************************************************************************
skipping: [167.172.129.114]

TASK [Upload new GrimoireLab settings] ******************************************************************************************************************************************
changed: [167.172.129.114] => (item=setup.cfg)
changed: [167.172.129.114] => (item=projects.json)

TASK [Start GrimoireLab containers] *********************************************************************************************************************************************
changed: [167.172.129.114]

TASK [Start GrimoireLab containers in GCP Containers Optimized instance] ********************************************************************************************************
skipping: [167.172.129.114]

PLAY RECAP **********************************************************************************************************************************************************************
167.172.129.114            : ok=6    changed=5    unreachable=0    failed=0    skipped=2    rescued=0    ignored=0
```

And after some time you'll start seeing some data in `http://<PUBLIC_IP>:5601`.

# Contributing

I have started with just a few projects in the [projects.json](projects.json) file.
If you think an important one is missing, feel free to submit a merge request to
add it.

If you have an specific question about any GNOME project, please, open an issue
to see if I am able to answer it with the data we cann collect with GrimoireLab.

Of course, if you are interested in funding this project to have proper deployment,
in a dedicated and maintained sever, [send me an email](mailto:jsmanrique@gmail.com) 
to see what we could do.

# License

This work is public under the [GPL v3.0](LICENSE) license.